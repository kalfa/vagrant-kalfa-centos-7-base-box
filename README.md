A simple centos-7 base box with additionally:
- MTA service disabled
- puppet agent installed, to be used provisioning
- aws cli and boto3 installed against centos' provided python3
- aws-auth script installed

